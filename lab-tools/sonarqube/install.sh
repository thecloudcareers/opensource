#!/bin/bash

if [ $(id -u) -ne 0 ]; then
  echo "You should be a root user to perform this command"
  exit 1
fi

ELV=$(rpm -qi basesystem | grep Release  | awk -F . '{print $NF}')
export OSVENDOR=$(rpm -qi basesystem  | grep Vendor | awk -F : '{print $NF}' | sed -e 's/^ //')

yum install epel-release -y

if [ "$ELV" == "el7" ]; then
    yum install java-11-openjdk unzip -y
    URL="https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.9.10.61524.zip"
elif [ "$ELV" == "el8" ]; then
    yum install java-17-openjdk unzip -y
    URL="https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-10.4.0.87286.zip"
fi

FILENAME=$(echo $URL | awk -F / '{print $NF}')
FOLDERNAME=$(echo $FILENAME | sed -e 's/.zip//g')

id sonar &>/dev/null
if [ $? -ne 0 ]; then
  useradd sonar
fi

cd /home/sonar
pkill java
rm -rf sonarqube
curl -s -o $FILENAME $URL
unzip $FILENAME
rm -f $FILENAME
mv $FOLDERNAME sonarqube

chown sonar:sonar sonarqube -R
curl -s https://gitlab.com/thecloudcareers/opensource/-/raw/master/lab-tools/sonarqube/sonar.service >/etc/systemd/system/sonarqube.service
systemctl daemon-reload
systemctl enable sonarqube
sed -i -e '/^RUN_AS_USER/ d' -e '/#RUN_AS_USER/ a RUN_AS_USER=sonar' /home/sonar/sonarqube/bin/linux-x86-64/sonar.sh
systemctl start sonarqube